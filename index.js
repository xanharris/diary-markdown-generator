const parser = require('todotxt-parser');
const fs = require('fs');
const { join } = require('path');
const moment = require('moment-timezone');
const readline = require('readline-sync');
const { google } = require('googleapis');
const csvParserSync = require('csv-parse/lib/sync');

const todoDateFilter = '2019-04';
const yearFilter = 2019;
const monthFilter = 3; // Zero index based
const diaryEntriesFolder = 'diary-entries/';

async function main() {
  var startDateTime = moment().year(yearFilter).month(monthFilter).startOf('month');
  var endDateTime = moment().year(yearFilter).month(monthFilter).endOf('month');
  var currentDateTime = moment(startDateTime);

  var calendarItems = [];
  var totalDay = startDateTime.daysInMonth();
  calendarItems[currentDateTime.format('Do, dddd')] = [];
  for (var i = 1; i < totalDay; i++) {
    var title = currentDateTime.add(1, 'd').format('Do, dddd');
    calendarItems[title] = [];
  }

  var data = fs.readFileSync('done.txt').toString().trim().replace(/\r/g, '');
  var tasks = parser.relaxed(data); // Bug: Works if file only contains completed todo entries (Cannot mix)
  var filtered = tasks.filter(item => (item.complete == true && item.dateCompleted.startsWith(todoDateFilter)));
  var todoItems = [];
  //var todoItemsForCalendar = [];
  for (const todo of filtered) {
    var dateOnly = todo.dateCompleted.substring(0, 10);
    var key = moment(dateOnly).format('Do, dddd');
    if (!todoItems[key]) {
      todoItems[key] = [];
    }

    todoItems[key].push(todo.text.replace(/(rec:.*|due:.*|t:.*|rem:.*)/, '').trim());
    //todoItemsForCalendar[key] = { date: dateOnly };
  };

  for (var title in todoItems) {
    var mergedTasks = '### Task\n\n';
    var counter = 0;
    var items = todoItems[title];
    items.forEach(function (value) {
      mergedTasks += `${++counter}. ${value}\n`;
    });

    todoItems[title] = mergedTasks;
    //todoItemsForCalendar[title].text = mergedTasks.replace(/^### Task\n\n/, '');
  }

  var workItems = [];
  data = fs.readFileSync('work.csv').toString();
  var items = csvParserSync(data, { columns: true, delimiter: '\t' });
  for (var i in items) {
    var row = items[i];
    var key = moment(row['Date Completed'], 'MM/DD/YYYY').format('Do, dddd');
    workItems[key] = workItems[key] || [];
    workItems[key].push(`${row.Subject} (Hours: ${row.Hours}) #${row.Categories}`);
  }

  for (var title in workItems) {
    var mergedTasks = '### Work\n\n';
    var counter = 0;
    var items = workItems[title];
    items.forEach(function (value) {
      mergedTasks += `${++counter}. ${value}\n`;
    });

    workItems[title] = mergedTasks;
  }

  var expenseItems = [];
  data = fs.readFileSync('expenses.csv').toString();
  var items = csvParserSync(data, { columns: true, delimiter: '\t' });
  for (var i in items) {
    var row = items[i];
    var key = moment(row.Date, 'D/M/YYYY').format('Do, dddd');
    expenseItems[key] = expenseItems[key] || [];
    expenseItems[key].push({
      amount: (row.Amount * -1),
      text: `|${row.Notes || row.Payee || row.Description}|$${(row.Amount * -1).toFixed(2)}|${row.Category}|`
    });
  }

  for (var title in expenseItems) {
    var mergedExpenses = '### Expenses\n\n|Description|Amount|Category|\n|-|-:|-|\n';
    var items = expenseItems[title];
    var totalAmount = 0.00;
    items.forEach(function (value) {
      mergedExpenses += `${value.text}\n`;
      totalAmount += value.amount;
    });

    mergedExpenses += `|**Total**|**$${totalAmount.toFixed(2)}**||\n`;
    expenseItems[title] = mergedExpenses;
  }

  if (fs.existsSync("diary.md")) {
    fs.truncateSync("diary.md");
  }

  var diaryFd = fs.openSync('diary.md', 'a');

  // If modifying these scopes, delete token.json.
  const SCOPES = ['https://www.googleapis.com/auth/calendar'];
  const TOKEN_PATH = 'token.json';

  // Load client secrets from a local file.
  if(!fs.existsSync('credentials.json')) {
    return console.log('Client secret file not found');
  }
  var content = fs.readFileSync('credentials.json')
  var oAuth2Client = await authorize(JSON.parse(content));
  var eventItems = await listEvents(oAuth2Client);

  var entries = fs.readdirSync(diaryEntriesFolder);
  for (const entry of entries) {
    var title = moment(entry.substring(0, 10)).format('Do, dddd');
    if (!calendarItems[title]) {
      continue;
    }
    calendarItems[title].push(fs.readFileSync(`${diaryEntriesFolder}/${entry}`).toString()  + '\n\n');
  }

  for (var title in eventItems) {
    calendarItems[title].push(eventItems[title]);
  }

  for (var title in expenseItems) {
    if (!calendarItems[title]) {
      continue;
    }
    calendarItems[title].push(expenseItems[title] + '\n');
  }
  
  for (var title in todoItems) {
    if (!calendarItems[title]) {
      continue;
    }
    calendarItems[title].push(todoItems[title] + '\n');
  }
  
  for (var title in workItems) {
    if (!calendarItems[title]) {
      continue;
    }
    calendarItems[title].push(workItems[title] + '\n');
  }

  // Final output
  for (var title in calendarItems) {
    var items = calendarItems[title];
    if (items.length) {
      fs.appendFileSync(diaryFd, `## ${title}\n\n`);
    }

    items.forEach(function (value) {
      // This condition is for legacy entries in 2018
      if (value.startsWith('### Diary Entry')) {
        value = value.replace(/^### Diary Entry\n\n/, '');
      }
      fs.appendFileSync(diaryFd, value);
    });
  }
  fs.closeSync(diaryFd);

  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  async function authorize(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    if(fs.existsSync(TOKEN_PATH)) {
      var token = fs.readFileSync(TOKEN_PATH);
      oAuth2Client.setCredentials(JSON.parse(token));
    }
    else {
      await getAccessToken(oAuth2Client);
    }
    return oAuth2Client;
  }

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback for the authorized client.
   */
  async function getAccessToken(oAuth2Client) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    var code = readline.question('Enter the code from that page here: ');
    var result = await oAuth2Client.getToken(code);
    var tokenData = result.res.data;
    oAuth2Client.setCredentials(tokenData);
    // Store the token to disk for later program executions
    fs.writeFileSync(TOKEN_PATH, JSON.stringify(tokenData));
    console.log('Token stored to', TOKEN_PATH);
  }

  /**
   * Lists the next 10 events on the user's primary calendar.
   * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
   */
  async function listEvents(auth) {
    const calendar = google.calendar({ version: 'v3', auth });
    const medicalHistory = '95r4k0tluiuph9118o151bogu0@group.calendar.google.com';
    const sharedLife = 'am86s8km784n3ebfq7ajm9s11s@group.calendar.google.com';
    
    var eventItems = []
    await addEvents(calendar, sharedLife, eventItems);
    await addEvents(calendar, medicalHistory, eventItems);

    for (var title in eventItems) {
      var mergedEvents = '### Events\n\n';
      var items = eventItems[title];
      items.forEach(function (value) {
        mergedEvents += `${value}\n`;
      });
  
      eventItems[title] = mergedEvents;
    }

    return eventItems;
  }

  async function addEvents(calApi, calId, eventList) {
    var response = await calApi.events.list({
      calendarId: calId,
      timeMin: startDateTime.toISOString(),
      timeMax: endDateTime.toISOString(),
      singleEvents: true,
      orderBy: 'startTime'
    });

    var events = response.data.items;
    if (events.length) {
      events.sort(function (a, b) {
        var nameA = a.summary.toLowerCase();
        var nameB = b.summary.toLowerCase();

        // This condition is for legacy entries in 2018
        // 1. Diary Entry must always be first
        if (nameA == 'diary entry') {
          return -1;
        }

        // This condition is for legacy entries in 2018
        if (nameB == 'diary entry') {
          return 1;
        }

        var momentA = moment.tz((a.start.dateTime || a.start.date), a.start.timeZone);
        var momentB = moment.tz((b.start.dateTime || b.start.date), b.start.timeZone);

        // 2. Sort by date
        if (momentA.isBefore(momentB)) {
          return -1;
        }
        if (momentA.isAfter(momentB)) {
          return 1;
        }

        // 3. Sort by name
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });

      for (var i in events) {
        var event = events[i];
        // This condition is for legacy entries in 2018
        if ((event.summary == 'Diary Entry') && (!event.description)) {
          continue;
        }

        const start = event.start.dateTime || event.start.date;
        var title = moment.tz(start, event.start.timeZone).format('Do, dddd');

        //console.log(`${event.id}: ${start} - ${event.summary}`);
        var item = '';
        item += `Title: **${event.summary}**\n`;
        if (event.start.dateTime) {
          var startTime = moment.tz(event.start.dateTime, event.start.timeZone);
          var endTime = moment.tz(event.end.dateTime, event.end.timeZone);
          var startDate = startTime.format('YYYYMMDD');
          var endDate = endTime.format('YYYYMMDD');
          var startTimeString = '';
          var endTimeString = '';
          if(startDate != endDate) {
            startTimeString = startTime.format('MMM Do, YYYY, HH:mm');
            endTimeString = endTime.format('MMM Do, YYYY, HH:mm');
          }
          else {
            startTimeString = startTime.format('HH:mm');
            endTimeString = endTime.format('HH:mm');
          }
          item += `Time: ${startTimeString} - ${endTimeString}\n`;
        }
        else { // Only has date, no time
          var startDate = moment(event.start.date);
          if(startDate.isBefore(startDateTime)) {
            title = startDateTime.format('Do, dddd');
          }
          var endDate = moment(event.end.date).subtract(1, 'd');
          if(!endDate.isSame(startDate)) {
            var startDateString = startDate.format('MMM Do, YYYY');
            var endDateString = endDate.format('MMM Do, YYYY');
            item += `Date: ${startDateString} - ${endDateString}\n`;
          }
        }

        if (event.location) {
          item += `Location: ${event.location}\n`;
        }
        if (event.description) {
          var description = event.description.trim();
          if(description.length > 0) {
            item += `Description: ${description}\n`;
          }
        }

        if (!calendarItems[title]) {
          continue;
        }
        eventList[title] = eventList[title] || [];
        eventList[title].push(item);
      };
    } else {
      console.log(`No events found for: ${calId}`);
    }
  }
}

main();